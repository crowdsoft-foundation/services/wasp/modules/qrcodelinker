import uuid

from flask import Flask, jsonify, request, send_file, redirect
import json
import io
import os

from config import Config
from db.qrcode import QRCode
from models.csqrcode import csqrcode


from pbglobal.pblib.api_calls import ApiCalls
import requests
from requests.auth import HTTPBasicAuth
from time import sleep


app = Flask(__name__)

@app.route('/generate_tmp_qrcode', methods=["GET"])
def generate_tmp_qrcode():
    qr_payload = request.args.get("payload")
    error_correct = request.args.get("error_correct")
    kind = request.args.get("kind","default")
    logo_url = request.args.get("logo_url", "")
    image = csqrcode().create_qr_code(qr_payload, error_correct, kind, logo_url)

    img_io = io.BytesIO()
    image.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')

@app.route('/get_qrcode/<qr_code_id>', methods=["GET"])
def get_qrcode(qr_code_id):
    result = Config.db_session.query(QRCode) \
        .filter(QRCode.qrcode_id == qr_code_id) \
        .one_or_none()

    if result is not None:
        qr_code_data = json.loads(result.data).get("data")
        qr_code_id = qr_code_data.get("qrcode_id")
        qr_payload = f"{os.getenv('APIGATEWAY_EXTERNAL_BASEURL')}/sqr/" + qr_code_id
        error_correct = qr_code_data.get("error_correct")
        kind = qr_code_data.get("kind")
        logo_url = qr_code_data.get("logo_url", "")

        image = csqrcode().create_qr_code(qr_payload, error_correct, kind, logo_url)

        img_io = io.BytesIO()
        image.save(img_io, 'JPEG', quality=70)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')
    else:
        return "nope", 403

@app.route('/sqr/<qr_code_id>', methods=["GET"])
def redirect_sqr(qr_code_id):
    result = Config.db_session.query(QRCode) \
        .filter(QRCode.qrcode_id == qr_code_id) \
        .one_or_none()

    if result is not None:
        data = json.loads(result.data)
        qr_code_data = data.get("data")
        qr_code_payload = qr_code_data.get("payload")
        if type(qr_code_payload) == dict and qr_code_payload.get("type") == "url":
            qr_payload = qr_code_payload.get("value")
            if qr_code_payload.get("guest_login"):
                login = "guest_" + data.get("correlation_id")
                password = data.get("correlation_id")

                url = os.getenv("INTERNAL_ACCOUNTMANAGER_URL") + "/newlogin"

                payload = json.dumps({
                    "username": login,
                    "password": password,
                    "roles": ["role.guest_user"]
                })
                headers = {
                    'Content-Type': 'application/json',
                    'X-Correlation-ID': data.get("correlation_id"),
                    'X-Consumer-Custom-Id': data.get("consumer_id"),
                    'X-Real-Ip': "qrcodelinker"
                }

                response = requests.request("POST", url, headers=headers, data=payload)
                login_data = {}

                for i in range(0, 10):
                    url = os.getenv("APIGATEWAY_INTERNAL_BASEURL") + "/login"
                    auth = HTTPBasicAuth(login, password)
                    payload = {}
                    headers = {}

                    response = requests.request("GET", url, headers=headers, data=payload, auth=auth)
                    if response.status_code == 200:
                        login_data = response.json()
                        break;
                    else:
                        sleep(1)

                apikey = login_data.get("apikey")
                consumer_id = login_data.get("consumer", {}).get("id")
                consumer_name = login_data.get("consumer_name")
                username = login_data.get("username")

                if "?" in qr_payload:
                    qr_payload = qr_payload + f"&apikey={apikey}&consumer_id={consumer_id}&username={username}&consumer_name={consumer_name}"
                else:
                    qr_payload = qr_payload + f"?apikey={apikey}&consumer_id={consumer_id}&username={username}&consumer_name={consumer_name}"
            response = redirect(qr_payload)
            response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
            response.headers['Pragma'] = 'no-cache'
            return response
        else:
            return "QR-Code is unkown or broken", 403

@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)
