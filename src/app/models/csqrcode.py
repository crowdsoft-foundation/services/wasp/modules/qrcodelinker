import io
import requests
import tempfile

import qrcode
from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.moduledrawers import RoundedModuleDrawer
from qrcode.image.styles.colormasks import RadialGradiantColorMask

class csqrcode():
    def __init__(self):
        pass

    def create_test_images(self, id):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=6,
            border=4,
        )
        qr.add_data(id)
        qr.make(fit=True)

        qr.make_image(fill_color="black", back_color="white").save("./static/qrcode_0.png")
        qr.make_image(image_factory=StyledPilImage, module_drawer=RoundedModuleDrawer()).save("./static/qrcode_1.png")
        qr.make_image(image_factory=StyledPilImage, color_mask=RadialGradiantColorMask()).save("./static/qrcode_2.png")
        qr.make_image(image_factory=StyledPilImage, embeded_image_path="/src/app/ressources/logo.png").save(
            "./static/qrcode_3.png")


    def create_qr_code(self, payload, error_correct="H", kind="default", logo_url=""):
        if error_correct == "H":
            error_correct = qrcode.constants.ERROR_CORRECT_H
        elif error_correct == "Q":
            error_correct = qrcode.constants.ERROR_CORRECT_Q
        elif error_correct == "M":
            error_correct = qrcode.constants.ERROR_CORRECT_M
        elif error_correct == "L":
            error_correct = qrcode.constants.ERROR_CORRECT_L
        else:
            error_correct = qrcode.constants.ERROR_CORRECT_H

        qr = qrcode.QRCode(
            version=1,
            error_correction=error_correct,
            box_size=6,
            border=4,
        )
        qr.add_data(payload)
        qr.make(fit=True)
        image = None
        if kind == "default":
            image = qr.make_image(fill_color="black", back_color="white")
        elif kind == "logo":
            data = requests.get(logo_url).content

            with tempfile.NamedTemporaryFile() as tmp:
                tmp.write(data)
                image = qr.make_image(image_factory=StyledPilImage, embeded_image_path=tmp.name)
                tmp.close()

        return image