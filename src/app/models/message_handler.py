import json
from time import sleep
import shortuuid
from pbglobal.pblib.amqp import client

import traceback

from pbglobal.events.qrCodeCreated import qrCodeCreated
from db.qrcode import QRCode
from config import Config
import hashlib

class message_handler():
    kong_api = "http://kong.planblick.svc:8001"
    queue_client = client()

    def handle_message(ch, method=None, properties=None, body=None):
        try:
            print("---------- Start handling Message ----------")
            event = method.routing_key
            body = body.decode("utf-8")
            instance = message_handler()
            if (event is not None):
                handler_exists = method.routing_key in dir(instance)

                if handler_exists is True:
                        if getattr(instance, method.routing_key)(ch, body) == True:
                            ch.basic_ack(method.delivery_tag)
                        else:
                            ch.basic_nack(method.delivery_tag)
                else:
                    raise Exception("No handler for this  message or even not responsible: '" + event + "'")

        except Exception as e:
            print(e)
            traceback.print_exc()
            ch.basic_nack(method.delivery_tag)
            sleep(15)

        print("---------- End handling Message ----------")

    def createQRCode(self, ch, msg):
        print("createQRCode BODY", msg, qrCodeCreated)
        json_payload = json.loads(msg)
        json_payload["data"]["data_hash"] = hashlib.sha256(json.dumps(json_payload.get("data")).encode("utf-8")).hexdigest()
        json_payload["data"]["qrcode_id"] = shortuuid.uuid()
        event = qrCodeCreated.from_json(qrCodeCreated, json_payload)
        event.publish()

        return True

    def qrCodeCreated(self, ch, msg):
        json_data = json.loads(msg)

        db_entry = Config.db_session.query(QRCode).filter_by(data_hash=json_data.get("data", {}).get("data_hash")).one_or_none()

        if db_entry is None:
            db_entry = QRCode()
            db_entry.consumer_id = json_data.get("consumer_id")
            db_entry.creator = json_data.get("creator")
            db_entry.owner_id = json_data.get("consumer_id")
            db_entry.qrcode_id = json_data.get("data").get("qrcode_id")
            db_entry.data = json.dumps(json_data)
            db_entry.data_owners = "[]"
            db_entry.create_time = json_data.get("create_time")
            db_entry.data_hash = json_data.get("data", {}).get("data_hash", "")
            db_entry.save()

        message = {"event": "qrCodeCreated",
                   "args": {"qr_code_id": db_entry.qrcode_id, "task_id": json_data.get("correlation_id")}}

        payload = {"room": json_data.get("consumer_id"), "data": message}
        client().publish(toExchange="socketserver", routingKey="events", message=json.dumps(payload))
        return True





