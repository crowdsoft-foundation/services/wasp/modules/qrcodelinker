##
import requests
import json
import os
from db.qrcode import QRCode
from db.migrate import migrate_db
from pbglobal.pblib.autorun_basic import AutorunBasic


class Autorun(AutorunBasic):

    def __init__(self):
        super(Autorun, self).__init__()

    def post_deployment(self):
        QRCode(create_table_if_not_exists=True)
        migrate_db()
        print("Executing initialisation-script")
        print(self.kong_api)
        if self.kong_api == "":
            return

        request = requests.get(self.kong_api)

        if request.status_code == 200:
            self.add_service("qrcodelinker", "http://qrcodelinker.planblick.svc:8000")
            self.add_route(service_name="qrcodelinker", path="/get_qrcode", strip_path=False, plugins=[], delete_before_create=True)
            self.add_route(service_name="qrcodelinker", path="/sqr", strip_path=False, plugins=[])
            self.add_route(service_name="qrcodelinker", path="/generate_tmp_qrcode", strip_path=False, delete_before_create=True)

        else:
            print("Kong-API not available:", self.kong_api)

