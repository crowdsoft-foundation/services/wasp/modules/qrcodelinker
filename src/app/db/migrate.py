from db.abstract import Migrate
from sqlalchemy import types
types.VARCHAR
def migrate_db():
    migration = Migrate()

    migration.add_column(table_name="qrcodes", column_name="data_hash", data_type=types.VARCHAR(255))
    migration.execute("Set unique key ", f"ALTER TABLE `qrcodes` ADD UNIQUE INDEX `data_hash` (`data_hash`) USING BTREE;")


